import { Component, OnInit } from '@angular/core';

declare const $;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
   // tslint:disable-next-line:only-arrow-functions
   $(function() {
      $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          { extend: 'copy', className: 'btn btn-primary glyphicon glyphicon-duplicate' },
          { extend: 'csv', className: 'btn btn-primary glyphicon glyphicon-save-file' },
          { extend: 'excel', className: 'btn btn-primary glyphicon glyphicon-list-alt' },
          { extend: 'pdf', className: 'btn btn-primary glyphicon glyphicon-file' },
          { extend: 'print', className: 'btn btn-primary glyphicon glyphicon-print' }
      ]
    });
    });
  }
}
